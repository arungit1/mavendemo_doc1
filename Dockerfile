FROM maven:3.6.3-openjdk-11 AS build

RUN apt-get update
RUN apt-get install git -y

RUN mkdir mavendemo
RUN cd mavendemo

RUN git clone https://gitlab.com/arungit1/mavendemo_jen.git
WORKDIR mavendemo_jen
RUN ls
RUN mvn clean package

ENTRYPOINT ["java","-jar","target/demomaven-0.0.3-SNAPSHOT.jar"]
